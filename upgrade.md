# Upgrade Postgresql

There is two utility that you can use :
- pg_upgrade :  Rapid upgrades by using old data and adding new features, not adapted to system tables changes
- pg_dump/pg_restore:

If you are using gitlab's embeded postgres database, you case use the command :
```
    gitlab-ctl pg-upgrade
```

## Bibliography :
- [Upgrading PostgreSQL 11 to PostgreSQL 13 with TimescaleDB and PostGIS in Linux using pg_upgrade](https://severalnines.com/database-blog/upgrading-postgresql-11-postgresql-13-timescaledb-and-postgis-linux-using-pgupgrade)
- [How to Perform a Major Version Upgrade Using pg_upgrade in PostgreSQL](https://blog.crunchydata.com/blog/how-to-perform-a-major-version-upgrade-using-pg_upgrade-in-postgresql)
- [Upgrade your PostgreSQL database using dump and restore](https://docs.microsoft.com/en-us/azure/postgresql/how-to-upgrade-using-dump-and-restore)
- [Gitlab Upgrades postgres if embeded](https://docs.gitlab.com/omnibus/settings/database.html#upgrade-packaged-postgresql-server)
- [Gitlab Upgrades postgres if not embeded](https://docs.gitlab.com/omnibus/settings/database.html#upgrade-a-non-packaged-postgresql-database)